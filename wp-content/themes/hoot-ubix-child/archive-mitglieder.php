<?php
// Loads the header.php template.
get_header();

function wpb_hook_javascript() {
    ?>
    <script type='text/javascript' src='<?php echo get_stylesheet_directory_uri() . "/js/filter.js" ?>'></script>
    <?php
}
add_action('wp_footer', 'wpb_hook_javascript');

// remove Archive: prefix todo: does not work can be removed; configured with yoast.
add_filter('get_the_archive_title', function ($title) {
    return preg_replace('/^\w+: /', '', $title);
});
// Dispay Loop Meta at top
hootubix_display_loop_title_content('pre', 'index.php');
if (hootubix_page_header_attop()) {
    get_template_part('template-parts/loop-meta'); // Loads the template-parts/loop-meta.php template to display Title Area with Meta Info (of the loop)
    hootubix_display_loop_title_content('post', 'index.php');
}

function get_cities($cities) {
    return implode(', ', array_column($cities['standorte'], 'ort'));
}

function get_states($states, $class = false) {
    $output = [];
    foreach ($states as $s) {
        $output[] = $class ? 'members-' . get_term($s, 'bundesland')->slug : get_term($s, 'bundesland')->name;
    }
    return implode($class ? " " : ", ", $output);
}


// get all active members
$members = $posts = get_posts([
    'post_type' => 'mitglieder',
    'post_status' => 'publish',
    'numberposts' => -1,
    'orderby' => 'post_title',
    'order' => 'ASC'
]);

// get the custom fields by post ids
$members_data = [];

foreach ($members as $k => $m) {
    $members_data[$k] = get_fields($m->ID);
    $members_data[$k]['ID'] = $m->ID;
}

// get the state ids for the filter-buttons
$states = [];

foreach($members_data as $md) {
    if (count($md['bundeslaender']) > 1) {
        foreach ($md['bundeslaender'] as $m) {
            if (!in_array($m, $states)) {
                $states[] = $m;
            }
        }
    } else {
        if (!in_array($md['bundeslaender'][0], $states)) {
            $states[] = $md['bundeslaender'][0];
        }
    }
}
sort($states);
?>
    <div class="hgrid main-content-grid">
        <main id="content" role="main" itemprop="mainContentOfPage" class="content no-sidebar layout-none">
            <div id="content-wrap">
                <div id="members-filters">
                    <ul class="members-filter-menu">
                        <!-- For filtering controls add -->
                        <li class="members-filter-all members-active"> Alle </li>
                        <?php
                        foreach ($states as $s):
                            ?>
                            <li class="<?php echo 'members-filter-' . get_term($s, 'bundesland')->slug ?>"><?php echo get_term($s, 'bundesland')->name ?></li>
                        <?php
                        endforeach;
                        ?>
                    </ul>
                </div>

                <div id="members-wrap">
                    <?php
                    for ($i = 0; $i < count($members_data); $i += 2):
                        ?>
                        <div class="members-item <?php echo get_states($members_data[$i]['bundeslaender'], true) ?>">
                            <div class="members-img">
                                <?php if ($members_data[$i]['webseite'] || $members_data[$i]['unterseite']): ?>
                                <a href="<?php echo $members_data[$i]['webseite'] ? $members_data[$i]['webseite'] : 'mitglieder/' . get_post($members_data[$i]['ID'])->post_name ?>"
                                   rel="noopener" <?php echo $members_data[$i]['webseite'] ? 'target="_blank"' : '' ?>>
                                <?php endif; ?>
                                <?php
                                $attr = [
                                    'src'    => $members_data[$i]['wappen']['url'],
                                    'srcset' => $members_data[$i]['wappen']['sizes']['members-img-1x'] . ', ' . $members_data[$i]['wappen']['sizes']['members-img-2x'] . ' 2x'
                                ];
                                if (!empty($members_data[$i]['wappen'])) {
                                    echo wp_get_attachment_image($members_data[$i]['wappen']['ID'], false, false, $attr);
                                } else {
                                    // todo: better image and sizes
                                    echo '<img src="' . get_stylesheet_directory_uri() . '/img/ddhf_member_small.jpg" loading="lazy">';
                                }
                                ?>
                                <?php if ($members_data[$i]['webseite'] || $members_data[$i]['unterseite']): ?>
                                </a>
                                <?php endif; ?>
                            </div>
                            <div class="members-information">
                                <p class="members-title"><?php echo $members_data[$i]['name'] ?></p>
                                <?php echo $members_data[$i]['standorte'][0]['ort'] ? 'Training in: ' . get_cities($members_data[$i]) . '<br>' : '' ?>
                                Land: <?php echo get_states($members_data[$i]['bundeslaender']); ?> <br><br>

                                <?php if ($members_data[$i]['webseite'] || $members_data[$i]['unterseite']): ?>
                                <div class="members-link">
                                    <span class="members-color">Webseite:</span> <a
                                            href="<?php echo $members_data[$i]['webseite'] ? $members_data[$i]['webseite'] : 'mitglieder/' . get_post($members_data[$i]['ID'])->post_name ?>"
                                            rel="noopener" <?php echo $members_data[$i]['webseite'] ? 'target="_blank"' : '' ?>><?php echo $members_data[$i]['webseite'] ? $members_data[$i]['webseite'] : get_home_url() . '/mitglieder/' . get_post($members_data[$i]['ID'])->post_name ?></a>
                                </div>
                                <?php endif; ?>

                                <?php if ($members_data[$i]['email']): ?>
                                    <div class="members-email">
                                        E-Mail: <?php echo str_replace('@', ' [at] ', $members_data[$i]['email']); ?>
                                    </div>
                                <?php endif; ?>
                            </div>

                        </div>
                        <?php
                        if ($i + 1 < count($members_data)):
                            ?>
                            <div class="members-item members-<?php echo get_term($members_data[$i + 1]['bundeslaender'][0], 'bundesland')->slug ?>">
                                <div class="members-img">
                                    <a href="<?php echo $members_data[$i + 1]['webseite'] ? $members_data[$i + 1]['webseite'] : 'mitglieder/' . get_post($members_data[$i + 1]['ID'])->post_name ?>"
                                       rel="noopener" <?php echo $members_data[$i + 1]['webseite'] ? 'target="_blank"' : '' ?>>
                                        <?php
                                        $attr = [
                                            'src'    => $members_data[$i + 1]['wappen']['url'],
                                            'srcset' => $members_data[$i + 1]['wappen']['sizes']['members-img-1x'] . ', ' . $members_data[$i + 1]['wappen']['sizes']['members-img-2x'] . ' 2x'
                                        ];
                                        if (!empty($members_data[$i + 1]['wappen'])) {
                                            echo wp_get_attachment_image($members_data[$i + 1]['wappen']['ID'], false, false, $attr);
                                        } else {
                                            // todo: better image and sizes
                                            echo '<img src="' . get_stylesheet_directory_uri() . '/img/ddhf_member_small.jpg" loading="lazy">';
                                        }
                                        ?>
                                    </a>
                                </div>
                                <div class="members-information">
                                    <p class="members-title"><?php echo $members_data[$i + 1]['name'] ?></p>
                                    <?php echo $members_data[$i + 1]['standorte'][0]['ort'] ? 'Training in: ' . get_cities($members_data[$i+1]) . '<br>' : '' ?>
                                    Land: <?php echo get_states($members_data[$i + 1]['bundeslaender']); ?> <br><br>

                                    <div class="members-link">
                                        <span class="members-color">Webseite:</span> <a
                                                href="<?php echo $members_data[$i + 1]['webseite'] ? $members_data[$i + 1]['webseite'] : 'mitglieder/' . get_post($members_data[$i + 1]['ID'])->post_name ?>"
                                                rel="noopener" <?php echo $members_data[$i + 1]['webseite'] ? 'target="_blank"' : '' ?>><?php echo $members_data[$i + 1]['webseite'] ? $members_data[$i + 1]['webseite'] : get_home_url() . '/mitglieder/' . get_post($members_data[$i + 1]['ID'])->post_name ?></a>
                                    </div>

                                    <?php if ($members_data[$i + 1]['email']): ?>
                                        <div class="members-email">
                                            E-Mail: <?php echo str_replace('@', ' [at] ', $members_data[$i + 1]['email']); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php
                        endif;
                        ?>
                    <?php
                    endfor;
                    ?>
                    <!-- foreach -->
                </div>
                <div class="members-request-box">
                    <section id="hoot-cta-widget-4" class="widget widget_hoot-cta-widget">
                        <div class="cta-widget-wrap topborder-none bottomborder-none">
                            <div class="cta-widget">
                                <h4 class="cta-headline">Vermisst ihr eure Fechtgruppe auf dieser Seite?</h4>
                                <a href="/mitgliedschaft" class="cta-widget-button  button button-medium border-box">Jetzt Mitgliedschaft beantragen</a>
                            </div>
                        </div>
                    </section>
                </div>
            </div><!-- #content-wrap -->
        </main><!-- #content -->
    </div><!-- .hgrid -->
<?php get_footer(); // Loads the footer.php template. ?>

