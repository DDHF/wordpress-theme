<?php
if(get_field('mathjax')) {
    add_action('wp_head', 'include_mathjax');
}
// Loads the header.php template.
get_header();
?>

<?php
// Dispay Loop Meta at top
hootubix_display_loop_title_content('pre', 'single.php');
if (hootubix_page_header_attop()) {
    get_template_part('template-parts/loop-meta'); // Loads the template-parts/loop-meta.php template to display Title Area with Meta Info (of the loop)
    hootubix_display_loop_title_content('post', 'single.php');
}

// Template modification Hook
do_action('hootubix_template_before_content_grid', 'single.php');
?>

    <div class="hgrid main-content-grid">

        <?php
        // Template modification Hook
        do_action('hootubix_template_before_main', 'single.php');
        ?>

        <main <?php hybridextend_attr('content'); ?>>

            <?php
            // Template modification Hook
            do_action('hootubix_template_main_start', 'single.php');

            // Checks if any posts were found.
            if (have_posts()) :

                // Dispay Loop Meta in content wrap
                if (!hootubix_page_header_attop()) {
                    hootubix_display_loop_title_content('post', 'single.php');
                    get_template_part('template-parts/loop-meta'); // Loads the template-parts/loop-meta.php template to display Title Area with Meta Info (of the loop)
                }
                ?>

                <div id="content-wrap">

                    <?php
                    // Template modification Hook
                    do_action('hootubix_loop_start', 'single.php');

                    // Display Featured Image if present
                    if (hootubix_get_mod('post_featured_image')) {
                        $img_size = apply_filters('hootubix_post_image_single', '');
                        hootubix_post_thumbnail('entry-content-featured-img', $img_size, true);
                    }

                    // Begins the loop through found posts, and load the post data.
                    while (have_posts()) : the_post();

                        // making sure that there actually is some content; otherwise it returns a whitespace structure
                        if (get_post()->post_content != '') {
                            // Loads the template-parts/content-{$post_type}.php template.
                            hybridextend_get_content_template();
                        }

                        // End found posts loop.
                    endwhile;

                    handle_downloads();

                    // Template modification Hook
                    do_action('hootubix_loop_end', 'single.php');
                    ?>
                </div><!-- #content-wrap -->

            <?php


            // If no posts were found.
            else :

                // Loads the template-parts/error.php template.
                get_template_part('template-parts/error');

                // End check for posts.
            endif;

            // Template modification Hook
            do_action('hootubix_template_main_end', 'single.php');
//                        echo "<pre>";var_dump($club);echo "</pre>";
            ?>
        </main><!-- #content -->

        <?php
        // Template modification Hook
        do_action('hootubix_template_after_main', 'single.php');
        // Loads the sidebar.php template.
        if (in_array(123, wp_list_pluck(get_the_terms(get_the_ID(), 'category'), 'term_id'))) {

            $posts = get_posts(['category' => 123, 'exclude' => [get_the_ID()]]);
            $club = get_fields(get_field('verein')[0]);

            ?>
            <aside id="sidebar-primary" class="sidebar sidebar-primary hgrid-span-3 layout-narrow-right "
                   role="complementary" itemscope="itemscope" itemtype="https://schema.org/WPSideBar">
                <?php if (!empty($club)): ?>
                <section id="hoot-posts-blocks-widget-21" class="widget widget_hoot-posts-blocks-widget">
                    <!-- @todo return author information; more than one author or club must be handled   -->
                    <div class="content-blocks-widget-wrap content-blocks-posts topborder-none bottomborder-none">
                        <div class="content-blocks-widget aside-align">
                            <h3 class="widget-title">Über den Verein</h3>
                            <div class="aside-img">
                                <a href="<?php echo $club['webseite'] ? $club['webseite'] : 'mitglieder/' . get_post($club['ID'])->post_name ?>"
                                   rel="noopener" <?php echo $club['webseite'] ? 'target="_blank"' : '' ?>>
                                    <img src="<?php echo $club['wappen']['sizes']['medium'] ? $club['wappen']['sizes']['medium'] : 'http://' . $_SERVER['SERVER_NAME'] . '/wp-content/uploads/2019/08/ddhf_small.jpg' ?>">
                                </a>
                            </div>
                            <div class="aside-title">
                                <?php echo $club['name'] ?>
                            </div>
                            <div class="aside-city">
                                <!-- @todo mehrere standorte -->
                                <?php echo $club['standorte'] ? implode(', ', wp_list_pluck($club['standorte'], 'ort')) : '' ?>
                            </div>
                        </div>
                    </div>
                </section>
                <?php endif; ?>
                <section id="hoot-posts-blocks-widget-21" class="widget widget_hoot-posts-blocks-widget">
                    <div class="content-blocks-widget-wrap content-blocks-posts topborder-none bottomborder-none">
                        <div class="content-blocks-widget">
                            <h3 class="widget-title">Weitere Beiträge</h3>
                            <div class="flush-columns">
                                <?php
                                foreach ($posts as $p):
                                ?>
                                <div class="content-block-row">
                                    <div class="content-block-column hcolumn-1-1 content-block-style2 visual-none">
                                        <div class="content-block no-highlight">
                                            <div class="content-block-content no-visual">
                                                <h4 class="content-block-title"><a
                                                            href="<?php the_permalink($p->ID) ?>"
                                                            class="content-block-link"><?php echo $p->post_title ?></a></h4>
                                                <div class="content-block-subtitle small">
                                                    <div class="entry-byline">
                                                        <div class="entry-byline-block entry-byline-date">
                                                            <time class="entry-published updated"
                                                                  datetime="<?php echo $p->post_date ?>"
                                                                  itemprop="datePublished">
                                                                <?php echo (new DateTime($p->post_date))->format('d.m.Y')  ?>
                                                            </time>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>

                </section>
            </aside>
            <?php
        } else {
            hybridextend_get_sidebar();
        }
        ?>
    </div><!-- .hgrid -->

<?php get_footer(); // Loads the footer.php template. ?>