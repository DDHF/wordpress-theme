<?php
// Loads the header.php template.
get_header();

// Dispay Loop Meta at top
hootubix_display_loop_title_content('pre', 'index.php');
if (hootubix_page_header_attop()) {
    get_template_part('template-parts/loop-meta-archive'); // Loads the template-parts/loop-meta.php template to display Title Area with Meta Info (of the loop)
    hootubix_display_loop_title_content('post', 'index.php');
}

function get_genderspecific_jobtitle($jobs, $current_job_slug) : string {
    $jobtitles = '';
    foreach ($jobs as $k => $job) {
        if ($current_job_slug == $job['slug'])
            $jobtitles = '<strong>' . $job['name'] . '</strong>';
    }
    return $jobtitles;
}

$default_image = get_stylesheet_directory_uri() . '/img/user.svg';
// collect data for all public staff members
// get the post ids
$staff = $posts = get_posts([
    'fields' => 'ids',
    'post_type' => 'ddhf_staff',
    'post_status' => 'publish',
    'numberposts' => -1,
]);

// get the categories
$job_categories = [];

// get the weight of each category
foreach (get_terms(['taxonomy' => 'ddhf_staff_category','hide_empty' => false]) as $key => $category) {
    $categories[$category->slug] = [
        'name'      => $category->name,
        'weight'    => get_field('gewichtung', 'ddhf_staff_category_' . $category->term_id),
        'jobs'      => []
    ];
}

// sort the categories by weight (asc) [and name (asc)]
if (!empty($categories)) {
    uasort($categories, function ($a, $b) {
        return $a['weight'] <=> $b['weight'] ?: $a['name'] <=> $b['name'];
    });
}

// get the jobs
foreach (get_terms(['taxonomy' => 'ddhf_staff_jobtitles','hide_empty' => false]) as $key => $jobtitle) {
    $jobtitle_acf = get_fields('ddhf_staff_jobtitles_' . $jobtitle->term_id);
    $categories[$jobtitle_acf['kategorie']->slug]['jobs'][$jobtitle->slug] = [
        'name'      => $jobtitle->name,
        'slug'      => $jobtitle->slug,
        'email'     => $jobtitle_acf['fallback-email'],
        'weight'    => $jobtitle_acf['gewichtung'],
        'held_by'   => []
    ];
}

// Sort the Subcategories
foreach ($categories as $key => $category) {
    uasort($categories[$key]['jobs'], function ($a, $b) {
        return $a['weight'] <=> $b['weight'] ?: $a['name'] <=> $b['name'];
    });
}

// preparing the staff data
foreach ($staff as $key => $post_id) {
    $post_acf = get_fields($post_id);
    // prepare the functions
    $functions = [];

    foreach ($post_acf['funktionen'] as $function) {
        // prepare the jobtitle
        $jobtitle_form = $post_acf['geschlecht'] == 'm' ?
            get_field('maennliche_titelform', 'ddhf_staff_jobtitles_' . $function->term_id) :
            ($post_acf['geschlecht'] == 'w' ?
                get_field('weibliche_titelform', 'ddhf_staff_jobtitles_' . $function->term_id) :
                $function->name);

        if (empty($jobtitle_form))
            $jobtitle_form = $function->name;

        $functions[] = [
            'name'              => $jobtitle_form,
            'slug'              => $function->slug,
            'weight'            => get_field('gewichtung', 'ddhf_staff_jobtitles_' . $function->term_id),
            'category'          => get_field('kategorie', 'ddhf_staff_jobtitles_' . $function->term_id)->name,
            'category_slug'     => get_field('kategorie', 'ddhf_staff_jobtitles_' . $function->term_id)->slug,
            'category_weight'   => get_field('gewichtung', 'ddhf_staff_category_' . get_field('kategorie', 'ddhf_staff_jobtitles_' . $function->term_id)->term_id)
        ];
    }

    $staff[$key] = [
        'first_name'    => $post_acf['vorname'],
        'last_name'     => $post_acf['nachname'],
        'email'         => $post_acf['email'],
        'club'          => [
            'name' => get_field('name', $post_acf['verein']->ID),
            'url'  => get_field('webseite', $post_acf['verein']->ID),
        ],
        'photo'         => $post_acf['foto'],
        'functions'     => $functions
    ];
}

foreach ($staff as $staffmember) {
    foreach ($staffmember['functions'] as $function) {
        $categories[$function['category_slug']]['jobs'][$function['slug']]['held_by'][] = $staffmember;
    }
}

// Sort by name
foreach ($categories as $k => $category) {
    foreach ($categories[$k]['jobs'] as $l => $job) {
        usort($categories[$k]['jobs'][$l]['held_by'], function ($a, $b) {
            $a_name = strtolower(
                    str_replace(
                            ['Ä', 'ä', 'Ü', 'ü', 'Ö', 'ö', 'ß'],
                            ['a', 'a', 'u', 'u', 'o', 'o', 'ss'],
                            trim($a['first_name']) . trim($a['last_name'])
                    )
            );
            $b_name = strtolower(
                    str_replace(
                            ['Ä', 'ä', 'Ü', 'ü', 'Ö', 'ö', 'ß'],
                            ['a', 'a', 'u', 'u', 'o', 'o', 'ss'],
                            trim($b['first_name']) . trim($b['last_name'])
                    )
            );
            return $a_name <=> $b_name;
        });
    }
}

?>
<div class="hgrid main-content-grid">
    <main id="content" role="main" itemprop="mainContentOfPage" class="content no-sidebar layout-none">
        <div id="content-wrap">
            <div id="org-chart">
                <?php foreach ($categories as $key => $category): ?>
                <?php if (!empty($category['jobs'])): ?>
                    <div class="org-chart-headline">
                        <h3><?php echo $category['name'] ?></h3>
                    </div>
                    <div class="org-chart-group-<?php echo $key ?>">
                        <div class="org-chart-chart">
                            <?php
                            foreach ($category['jobs'] as $i => $job):
                                $array_keys = array_keys($category['jobs']);
                                $next_key = array_search($i, $array_keys) + 1;
                                $next_key_exists = array_key_exists($next_key, $array_keys);
                                $prev_key = array_search($i, $array_keys) - 1;
                                $prev_key_exists = array_key_exists($prev_key, $array_keys);


                                /*
                                 * We need a new row when:
                                 * 1. It is the first element
                                 * 2. The current element has a different weight then the last element
                                 */
                                if ($i == array_key_first($category['jobs']) || ($prev_key_exists && $job['weight'] != $category['jobs'][$array_keys[$prev_key]]['weight'])):
                                ?>
                            <div class="org-chart-row">
                            <?php endif; ?>
                            <?php if (!empty($job['held_by'])): ?>
                            <?php foreach($job['held_by'] as $person): ?>
                                <div class="org-chart-item">
                                    <?php
                                    if (!empty($person['photo'])):
                                        $attr = [
                                            'src'    => $person['photo']['sizes']['org-chart-img-2x'],
                                            'srcset' => $person['photo']['sizes']['org-chart-img-1x'] . ', ' . $person['photo']['sizes']['org-chart-img-2x'] . ' 2x',
                                            'class'  => 'org-chart-img'
                                        ];
                                        echo wp_get_attachment_image($person['photo']['ID'], [160,160], false, $attr);
                                    else:
                                    ?>
                                        <img src="<?php echo $default_image ?>" />
                                    <?php
                                    endif;
                                    ?>
                                    <div class="org-chart-information">
                                        <div class="org-chart-name">
                                            <?php echo $person['first_name'] . ' ' . $person['last_name'] ?>
                                        </div>
                                        <div class="org-chart-link">
                                            <a href="<?php echo $person['club']['url'] ?>" target="_blank" rel="noopener noreferrer" title="<?php echo $person['club']['name']  ?>"><?php echo $person['club']['name'] ?></a>
                                        </div>
                                        <div class="org-chart-position">
                                            <?php echo get_genderspecific_jobtitle($person['functions'], $job['slug'])  ?>
                                        </div>
                                        <div class="org-chart-mail">
                                            <?php
                                                echo str_replace('@', ' [at] ', $person['email'] ?: $job['email'])
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach;
                                else: ?>
                                    <div class="org-chart-item">
                                        <img src="<?php echo $default_image ?>" class="org-chart-img" />
                                        <div class="org-chart-information">
                                            <div class="org-chart-vacant">
                                                Vakanz
                                            </div>
                                            <div class="org-chart-name">
                                                Position vakant
                                            </div>
                                            <div class="org-chart-link">
                                                <a href="/verband/bewerbung/">jetzt bewerben</a>
                                            </div>
                                            <div class="org-chart-position">
                                                <strong><?php echo $job['name'] ?></strong>
                                            </div>
                                            <div class="org-chart-mail">
                                                <?php echo str_replace('@', ' [at] ', $job['email'])  ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                    endif;
                                /*
                                * We need to close a row when:
                                * 1. It is the last element
                                * 2. The current element has a different weight then the last element
                                */
                                if ($i == array_key_last($category['jobs']) || ($next_key_exists && $job['weight'] != $category['jobs'][$array_keys[$next_key]]['weight'])):
                                ?>
                                </div>
                                    <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div><!-- #content-wrap -->
    </main><!-- #content -->
</div><!-- .hgrid -->
<?php get_footer(); // Loads the footer.php template. ?>

