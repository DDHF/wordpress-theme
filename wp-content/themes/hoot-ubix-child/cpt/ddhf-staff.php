<?php

function ddhf_register_cpt_staff() {
    register_post_type('ddhf_staff',
        [
            'labels'                => [
                'name'          => __('Personal', 'textdomain'),
                'singular_name' => __('Personal', 'textdomain'),
            ],
            'description'           => 'Mitglieder des Präsidiums, Leute in Gremien und Ämtern, die auf der Seite angezeigt werden sollen.',
            'public'                => true,
            'has_archive'           => true,
            'supports'              => [
                'title', 'revisions'
            ],
            'rewrite'               => [
                'slug'  => 'verband/personal',
            ],
            'publicly_queryable'    => true,
            'menu_position'         => 21,
            'menu_icon'             => 'dashicons-id-alt',
            'taxonomies'            => [
                'ddhf_staff_category'
            ],
            'show_in_menu' => false
        ]
    );
}
add_action('init', 'ddhf_register_cpt_staff');

function ddhf_register_tax_category() {
    register_taxonomy('ddhf_staff_category', 'ddhf_staff',
        [
            'labels'                => [
                'name'          => __('Personalkategorien', 'taxonomy general name'),
                'singular_name' => __('Personalkategorie', 'taxonomy singular name'),
                'all_items' => 'Alle Kategorien anzeigen',
                'new_item_name' => 'Neuen Namen festlegen',
                'edit_item' => 'Kategorie bearbeiten',
                'view_item' => 'Kategorie ansehen',
                'update_item' => 'Kategorie aktualisieren',
                'add_new_item' => 'Neue Kategorie anlegen',
                'search_items' => 'Kategorien durchsuchen',
                'popular_items' => 'Häufige Kategorien',
                'separate_items_with_commas' => 'Mehrere Kategorien mit Komma trennen',
                'add_or_remove_items' => 'Kategorie erstellen oder löschen',
                'choose_from_most_used' => 'Aus häufigen Kategorien wählen',
                'not_found' => 'Keine passende Kategorie gefunden',
                'back_to_items' => 'Zurück zur Übersicht',
            ],
            'description' => 'Kategorie(n), in denen die Person angezeigt werden soll.',
            'public' => true,
            'rewrite' => [
                'slug' => 'verband/personal/kategorien'
            ],
            'show_ui' => true,
            'show_in_menu' => false,
            'show_in_nav_menus' => false,
            'show_tagcloud' => false,
        ]
    );
}
add_action('init', 'ddhf_register_tax_category');

function ddhf_register_tax_jobtitles() {
    register_taxonomy('ddhf_staff_jobtitles', 'ddhf_staff',
        [
            'labels'                => [
                'name'          => __('Posten im DDHF', 'taxonomy general name'),
                'singular_name' => __('Posten im DDHF', 'taxonomy singular name'),
                'all_items' => 'Alle Posten anzeigen',
                'new_item_name' => 'Neuen Namen festlegen',
                'edit_item' => 'Posten bearbeiten',
                'view_item' => 'Posten ansehen',
                'update_item' => 'Posten aktualisieren',
                'add_new_item' => 'Neue Posten anlegen',
                'search_items' => 'Posten durchsuchen',
                'popular_items' => 'Häufige Posten',
                'separate_items_with_commas' => 'Mehrere Posten mit Komma trennen',
                'add_or_remove_items' => 'Posten erstellen oder löschen',
                'choose_from_most_used' => 'Aus häufigen Posten wählen',
                'not_found' => 'Keine passende Posten gefunden',
                'back_to_items' => 'Zurück zur Übersicht',
            ],
            'description' => 'Posten(n), die eine Person innehaben kann.',
            'public' => true,
            'rewrite' => [
                'slug' => 'verband/personal/posten'
            ],
            'show_ui' => true,
            'show_in_menu' => false,
            'show_in_nav_menus' => false,
            'show_tagcloud' => false,
        ]
    );
}
add_action('init', 'ddhf_register_tax_jobtitles');

/**
 * Generate backend menu for this cpt
 */
function staff_menu() {
    add_menu_page    ('Personal', 'Personal', 'edit_posts',
        'edit.php?post_type=ddhf_staff', '', 'dashicons-id-alt', 22 );
    add_submenu_page ('edit.php?post_type=ddhf_staff', 'Posten', '- Posten', 'edit_posts',
        'edit-tags.php?taxonomy=ddhf_staff_jobtitles&post_type=ddhf_staff', '', 10 );
    add_submenu_page ('edit.php?post_type=ddhf_staff', 'Kategorien', '-- Kategorien', 'edit_posts',
        'edit-tags.php?taxonomy=ddhf_staff_category&post_type=ddhf_staff', '', 20 );
}
add_action( 'admin_menu', 'staff_menu' );

/**
 * Making sure the submenu for ddhf_staff stays open, when on submenu pages
 */
function staff_menu_parent_file($parent_file) {
    global $current_screen, $self;
    if (in_array($current_screen->base, ['edit-tags', 'post']) && (in_array($current_screen->taxonomy, ['ddhf_staff_jobtitles', 'ddhf_staff_category']) || $current_screen->post_type == 'ddhf_staff')) {
        $parent_file = 'edit.php?post_type=ddhf_staff';
    }
    return $parent_file;
}
add_filter( 'parent_file', 'staff_menu_parent_file' );