Date.prototype.addDays = function (days) {
    return new Date(this.valueOf() + days * 864e5);
};
/*
 * shows the newsletter registration prompt
 */
function initPrompt() {
    const daysPromptHidden = 7;

    // check expire date of prompt
    if (
        new Date(localStorage.getItem('newsletterPrompt')) < new Date().addDays(-daysPromptHidden)
    ) {
        // remove if expired
        localStorage.removeItem('newsletterPrompt');
        // newsletter prompt is allowed to show up again
        // show prompt when scrolled down
        window.addEventListener('scroll', showPrompt);
    }
}

function showPrompt() {
    if (window.pageYOffset >= 100) {
        // remove scroll listener
        window.removeEventListener('scroll', showPrompt);
        // show prompt
        document.querySelector('.newsletterPrompt').classList.add('show');
    }
}

function closePrompt(submitted) {
    // close prompt
    const newsletterPrompt = document.querySelector('.newsletterPrompt');
    newsletterPrompt.classList.remove('show');

    if (!submitted) {
        // set current date to track next appearance
        localStorage.setItem('newsletterPrompt', new Date());
    } else {
        // set date in future to prevent prompt from opening again
        localStorage.setItem('newsletterPrompt', new Date('2099-12-31T23:59:59.000Z'));
    }
}

jQuery(document).ready(initPrompt);