<?php
// Loads the header.php template.
get_header();
?>

<?php
// Dispay Loop Meta at top
hootubix_display_loop_title_content('pre', 'single.php');
if (hootubix_page_header_attop()) {
    get_template_part('template-parts/loop-meta'); // Loads the template-parts/loop-meta.php template to display Title Area with Meta Info (of the loop)
    hootubix_display_loop_title_content('post', 'single.php');
}

// Template modification Hook
do_action('hootubix_template_before_content_grid', 'single.php');
?>

    <div class="hgrid main-content-grid">

        <?php
        // Template modification Hook
        do_action('hootubix_template_before_main', 'single.php');
        ?>

        <main <?php hybridextend_attr('content'); ?>>

            <?php
            // Template modification Hook
            do_action('hootubix_template_main_start', 'single.php');

            // Checks if any posts were found.
            if (have_posts()) :

                // Dispay Loop Meta in content wrap
                if (!hootubix_page_header_attop()) {
                    hootubix_display_loop_title_content('post', 'single.php');
                    get_template_part('template-parts/loop-meta'); // Loads the template-parts/loop-meta.php template to display Title Area with Meta Info (of the loop)
                }
                ?>

                <div id="content-wrap">

                    <?php
                    // Template modification Hook
                    do_action('hootubix_loop_start', 'single.php');
                    function cmp($a, $b)  {
                        return $b['points'] - $a['points'];
                    }
                    $tournaments = get_field('tournaments');

                    // get the relevant tournaments' results
                    $results = [];
                    if (is_array($tournaments)) {
                        foreach ($tournaments as $t) {
                            $results[] = get_fields($t->ID);
                        }
                    } else {
                        $results[] = get_fields($t->ID);
                    }

                    // reduce to important values and split between tournament-results and ranking points
                    $ranking_list = [];
                    $fencer_ids = [];

                    foreach ($results as $r) {
                        foreach ($r['results'] as $f) {
                            //check if fencer is already in array
                            if(!in_array($f['fencers'][0]->ID, $fencer_ids, false)) {
                                $fencer_ids[] = $f['fencers'][0]->ID;
                                $ranking_list[] = [
                                    'fencer_id' => $f['fencers'][0]->ID,
                                    'rank' => 0,
                                    'name' => get_field('name', $f['fencers'][0]->ID),
                                    'club' => get_term(get_field('club', $f['fencers'][0]->ID), 'club')->name,
                                    'points' => $f['points']
                                ];
                            } else {
                                $ranking_list[array_search($f['fencers'][0]->ID, $ranking_list)]['points'] += $f['points'];
                            }

                        }
                    }
                    // order the fencers desc by points
                    usort($ranking_list, 'cmp');

                    // determine each fencers rank
                    $rank = 0;
                    foreach ($ranking_list as $k =>$rl) {
                        if ($rank === 0) {
                            $ranking_list[$k]['rank'] = ++$rank;
                        } else {
                            if ($rl['points'] != $ranking_list[$k-1]['points']) {
                                $ranking_list[$k]['rank'] = ++$rank;
                            } else {
                                $ranking_list[$k]['rank'] = $rank;
                            }
                        }
                    }

                    // Template modification Hook
                    do_action('hootubix_loop_end', 'single.php');

                    // creating the table

                    if (count($ranking_list) > 0):
                        ?>
                        <table>
                            <thead>
                            <tr>
                                <th><?php echo implode('</th><th>', array_keys(current($ranking_list))); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($ranking_list as $row): array_map('htmlentities', $row); ?>
                                <tr>
                                    <td><?php echo implode('</td><td>', $row); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>

                </div><!-- #content-wrap -->

                <?php
                // Loads the template-parts/loop-nav.php template.
                if (hootubix_get_mod('post_prev_next_links'))
                    get_template_part('template-parts/loop-nav');

                // Template modification Hook
                do_action('hootubix_template_after_content_wrap', 'single.php');

                // Loads the comments.php template
                if (!is_attachment()) {
                    comments_template('', true);
                };

            // If no posts were found.
            else :

                // Loads the template-parts/error.php template.
                get_template_part('template-parts/error');

                // End check for posts.
            endif;

            // Template modification Hook
            do_action('hootubix_template_main_end', 'single.php');
            ?>

        </main><!-- #content -->

        <?php
        // Template modification Hook
        do_action('hootubix_template_after_main', 'single.php');
        ?>

        <?php hybridextend_get_sidebar('primary'); // Loads the template-parts/sidebar-primary.php template. ?>

    </div><!-- .hgrid -->

<?php get_footer(); // Loads the footer.php template. ?>