<?php
/**
 * Template to display single post content on archive pages
 * Archive Post Style: Big Thumbnail (default)
 */
?>

<article <?php hybridextend_attr( 'post', '', 'archive-big' ); ?>>

	<div class="entry-grid hgrid">

		<?php
        $highlight_image = get_field('beitragsbild');
        ?><div class="image-container-archive"><?php
		if ( !empty($highlight_image) ) {
            $attr = [
                'src'    => $highlight_image['sizes']['highlight-img']
            ];
            echo wp_get_attachment_image($highlight_image['ID'], [230, 121], false, $attr);
        }
        elseif (!empty(get_post_thumbnail_id())) {
            the_post_thumbnail();
        }
        else {
            echo '<img src="/wp-content/themes/hoot-ubix-child/img/logo-ddhf.jpg" alt="" loading="lazy" width="1200" height="630">';
        }
        ?>
        </div>
		<div class="entry-grid-content hgrid-span-12">

			<header class="entry-header">
				<?php the_title( '<h2 ' . hybridextend_get_attr( 'entry-title' ) . '><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" itemprop="url">', '</a></h2>' ); ?>
			</header><!-- .entry-header -->

			<?php if ( is_sticky() ) : ?>
				<div class="entry-sticky-tag invert-typo"><?php _e( 'Sticky', 'hoot-ubix' ) ?></div>
			<?php endif; ?>

			<div class="screen-reader-text" itemprop="datePublished" itemtype="https://schema.org/Date"><?php echo get_the_date('Y-m-d'); ?></div>
			<?php hootubix_meta_info_blocks( hootubix_get_mod('archive_post_meta'), 'archive-big' ); ?>

			<?php
			$archive_post_content = hootubix_get_mod('archive_post_content');
			if ( 'full-content' == $archive_post_content ) {
				?><div <?php hybridextend_attr( 'entry-summary', 'content' ); ?>><?php
					the_content();
				?></div><?php
				wp_link_pages();
			} elseif ( 'excerpt' == $archive_post_content ) {
				?><div <?php hybridextend_attr( 'entry-summary', 'excerpt' ); ?>><?php
					the_excerpt();
				?></div><?php
			}
			?>

		</div><!-- .entry-grid-content -->

	</div><!-- .entry-grid -->

</article><!-- .entry -->