<?php
		// Template modification Hook
		do_action( 'hootubix_template_main_wrapper_end' );
		?>
		</div><!-- #main -->

		<?php get_template_part( 'template-parts/footer', 'subfooter' ); // Loads the template-parts/footer-subfooter.php template. ?>

		<?php get_template_part( 'template-parts/footer', 'footer' ); // Loads the template-parts/footer-footer.php template. ?>

		<?php get_template_part( 'template-parts/footer', 'postfooter' ); // Loads the template-parts/footer-postfooter.php template. ?>

	</div><!-- #page-wrapper -->

	<?php wp_footer(); // WordPress hook for loading JavaScript, toolbar, and other things in the footer. ?>

    <?php if (!is_front_page()): ?>

        <div class="newsletterPrompt">
        <span>
            <i class="fas fa-times" onclick="closePrompt()"></i>
            <strong>Newsletter</strong>
        </span>
            <p>Melde dich für den offiziellen DDHF-Newsletter an und bleibe stets auf dem Laufenden!</p>
            <p><a href="/newsletterregistrierung" class="cta-widget-button button button-medium border-box" onclick="closePrompt()">Jetzt registrieren</a></p>
        </div>
    <?php else: ?>
        <script>
            // todo: remove after better solution
            let highlightElements = document.getElementsByClassName('content-block-content-hasicon');
            let heightMax = '0';
            for (let i = 0; i < highlightElements.length; i++) {
                if (highlightElements.item(i).offsetHeight > heightMax) {
                    heightMax = highlightElements.item(i).offsetHeight;
                }
            }
            for (let i = 0; i < highlightElements.length; i++) {
                highlightElements.item(i).style.minHeight = (heightMax + "px");
            }
        </script>
    <?php endif; ?>
</body>

</html>