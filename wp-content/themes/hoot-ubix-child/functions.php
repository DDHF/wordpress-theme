<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

include('includes/custom-shortcodes.php');
include('includes/custom-crops.php');

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'child_theme_configurator_css' ) ):
    function child_theme_configurator_css() {
        wp_enqueue_style( 'chld_thm_cfg_separate', trailingslashit( get_stylesheet_directory_uri() ) . 'ctc-style.css', array( 'hybridextend-template-style','hybridextend-style' ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css', 22 );

// END ENQUEUE PARENT ACTION

add_filter('page_attributes_dropdown_pages_args', 'my_attributes_dropdown_pages_args', 1, 1);

// allows drafts and private pages to be used as parent-pages
function my_attributes_dropdown_pages_args($dropdown_args) {

    $dropdown_args['post_status'] = array('publish','draft', 'private');

    return $dropdown_args;
}

/**
 * Integrate open graph tags
 */


add_action( 'wp_head', 'kb_load_open_graph' );

function kb_load_open_graph() {
    global $post;
    // Standard-Grafik für Seiten ohne Beitragsbild
    $kb_site_logo = get_stylesheet_directory_uri() . '/img/logo-ddhf.jpg';

    // Wenn Startseite
    if ( is_front_page() ) { // Alternativ is_home
        echo '<meta property="og:type" content="website" />' . "\n";
        echo '<meta property="og:url" content="' . get_bloginfo( 'url' ) . '" />' . "\n";
        echo '<meta property="og:title" content="' . esc_attr( get_bloginfo( 'name' ) ) . '" />' . "\n";
        echo '<meta property="og:image" content="' . $kb_site_logo . '" />' . "\n";
        echo '<meta property="og:description" content="' . esc_attr( get_bloginfo( 'description' ) ) . '" />' . "\n";
    }

    // Wenn Einzelansicht von Seite, Beitrag oder Custom Post Type
    elseif ( is_singular() ) {
        echo '<meta property="og:type" content="article" />' . "\n";
        echo '<meta property="og:url" content="' . get_permalink() . '" />' . "\n";
        echo '<meta property="og:title" content="' . esc_attr( get_the_title() ) . '" />' . "\n";
        if ($image = get_field('beitragsbild')) {
            echo '<meta property="og:image" content="' . esc_attr( $image['url'] ) . '" />' . "\n";
        }
        elseif ( has_post_thumbnail( $post->ID ) ) {
            $kb_thumbnail = get_the_post_thumbnail_url( $post->ID );
            echo '<meta property="og:image" content="' . esc_attr( $kb_thumbnail ) . '" />' . "\n";
        } else {
            echo '<meta property="og:image" content="' . $kb_site_logo . '" />' . "\n";
        }
        $excerpt = get_the_excerpt();
        echo '<meta property="og:description" content="' . esc_attr( substr($excerpt, 0, strpos($excerpt, '<span')) ) . '" />' . "\n";
    }
}

/**
 * add meta tag description
 */

add_action( 'wp_head', 'load_meta_description' );

function load_meta_description() {
    // Wenn Startseite
    if ( is_front_page() ) { // Alternativ is_home
        echo '<meta name="description" content="' . esc_attr( get_bloginfo( 'description' ) ) . '" />' . "\n";
    }

    // Wenn Einzelansicht von Seite, Beitrag oder Custom Post Type
    elseif ( is_singular() ) {
        $excerpt = get_the_excerpt();
        echo '<meta name="description" content="' . esc_attr( substr($excerpt, 0, strpos($excerpt, '<span')) ) . '" />' . "\n";
    }
}

/**
 * add javascript for newsletter prompt
 */

function javascript_newsletterprompt() {
    wp_register_script(
        'newsletterprompt',
        get_stylesheet_directory_uri() . '/js/newsletterPrompt.js',
        ['jquery']
    );
    wp_enqueue_script( 'newsletterprompt' );
}
add_action('wp_footer', 'javascript_newsletterprompt');

/**
 * Register custom post types
 */

require_once 'cpt/ddhf-staff.php';

/**
 * Adds an archive link into page-sitemap.xml (for selected post type)
 *
 * @see https://github.com/Yoast/wordpress-seo/issues/11391
 * @return string
 */
function add_archive_URL() {
    $post_type = 'ddhf_staff';
    $archive_url = get_post_type_archive_link( $post_type );

    $args = [
        'posts_per_page'         => 1,
        'post_type'              => $post_type,
        'orderby'                => 'modified',
        'order'                  => 'DESC',
        'no_found_rows'          => true,
        'update_post_meta_cache' => false,
        'update_post_term_cache' => false,
        'fields'                 => 'ids',
        'cache_results'          => true
    ];

    $latest = new WP_Query( $args );

    while ( $latest->have_posts() ) {
        $latest->the_post();
        $date = get_post_modified_time( 'Y-m-d h:i:s', true );
        $last_mod = YoastSEO()->helpers->date->format( $date );
    }

    $url = "\t<url>\n";
    $url .= "\t\t<loc>$archive_url</loc>\n";
    $url .= "\t\t<lastmod>$last_mod</lastmod>\n";
    $url .= "\t</url>\n";

    return $url;

}
add_filter( 'wpseo_sitemap_page_content', 'add_archive_URL' );

function hootubix_theme_menu_description( $title, $item, $args, $depth ) {

    $return = '';
    $return .= '<span class="menu-title">' . $title . '</span>';

    return $return;
}

add_filter('jpeg_quality', function($arg){return 90;});
