<?php
/**
 * List View Title Template
 * The title template for the list view of events.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/title-bar.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 * @since   4.6.19
 *
 */
?>

<div id="loop-meta" class="loop-meta-wrap pageheader-bg-default">
    <div class="hgrid">

        <div class=" loop-meta  hgrid-span-12" itemscope="itemscope" itemtype="https://schema.org/WebPageElement">
            <div class="entry-header">

                <h1 class=" loop-title" itemprop="headline"><?php echo tribe_get_events_title() ?></h1>

            </div><!-- .entry-header -->
        </div><!-- .loop-meta -->

    </div>
</div>