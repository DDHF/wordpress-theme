<?php

// images-sizes for the organizer chart
add_image_size( 'org-chart-img-1x', 160, 160 );
add_image_size( 'org-chart-img-2x', 320, 320 );

// image-sizes for the members area
add_image_size( 'members-img-1x', 130, 130 );
add_image_size( 'members-img-2x', 260, 260 );

// image-sizes for the post highlight image
add_image_size('highlight-img', 1200, 630);

// image-sizes for the download thumbnail
add_image_size('download-preview-1x', 0, 75);
add_image_size('download-preview-2x', 0, 150);