<?php

include "custom-helper.php";

/**
 * This function gets the legal representatives for the DDHF from the database und returns them as string starting with
 * the president and followed by the other main presidium-members.
 *
 * @return string
 */
function representatives_function() {

    $id_presidium = 199;
    $names = [];

    $args = array(
        'post_type'      => 'ddhf_staff',
        'post_status'    => 'publish',
        'posts_per_page' => -1
    );

    $loop = new WP_Query( $args );

    while ( $loop->have_posts() ) : $loop->the_post();
        $staff_members = get_fields(get_the_ID());

        foreach ($staff_members['funktionen'] as $function) {
            $job_category = get_field('kategorie', 'ddhf_staff_category_' . $function->term_id);

            if ($job_category->term_id == $id_presidium) {

                switch ($staff_members['geschlecht']) {
                    case 'w':
                        $title = get_field('weibliche_titelform', 'ddhf_staff_jobtitles_' . $function->term_id);
                        break;
                    case 'm':
                        $title = get_field('maennliche_titelform', 'ddhf_staff_jobtitles_' . $function->term_id);
                        break;
                    default:
                        $title = $function->name;
                }
                $names[] = [
                    'name' => $staff_members['vorname'] . ' ' . $staff_members['nachname'],
                    'title' => $title
                ];
            }
        }
    endwhile;

    usort($names, function($a, $b) {
        return $a['title'] <=> $b['title'];
    });
    wp_reset_postdata();

    foreach ($names as $key => $name) {
        $names[$key] = $name['name'] . ' (' . $name['title'] . ')';
    }

    // @todo add default value if there is no representative due to unforeseen circumstances
    return implode('<br>', $names);
}

add_shortcode('vertreter', 'representatives_function');

/**
 * This function creates a table for ranking results;
 * @todo catch errors Warning: array_keys() expects parameter 1 to be array, boolean given in /www/htdocs/w019bfc1/dev-ddhf.lautregister.eu/wp-content/themes/hoot-ubix-child/includes/custom-shortcodes.php on line 106 Warning: implode(): Invalid arguments passed in /www/htdocs/w019bfc1/dev-ddhf.lautregister.eu/wp-content/themes/hoot-ubix-child/includes/custom-shortcodes.php on line 106
 * @param $param []
 * @return string
 */
function rankingtable_function($param) {
    // set default values
    $param =
        shortcode_atts(
            array (
                'id' => 0,
                'anzahl' => 0, // default: everyone
                'kopfzeile' => 1 // default: display on
            ), $param
        );
    $html = "Es liegen leider noch keine Ergebnisse vor.";

    // get raw data
    $tournaments = get_field('tournaments', $param['id']);

    if (!empty($tournaments)) {
        // get the relevant tournaments' results
        $results = [];
        if (is_array($tournaments)) {
            foreach ($tournaments as $t) {
                $results[] = get_fields($t->ID);
            }
        } else {
            $results[] = get_fields($tournaments->ID);
        }
        // reduce to important values and split between tournament-results and ranking points
        $ranking_list = [];
        $fencer_ids = [];

        foreach ($results as $r) {
            if ($r['results']) {
                foreach ($r['results'] as $f) {
                    //check if fencer is already in array
                    if (!empty($f['fencers'][0])) {
                        if (!in_array($f['fencers'][0]->ID, $fencer_ids, false)) {
                            $fencer_id = $f['fencers'][0]->ID;
                            $fencer_ids[] = $fencer_id;

                            $ranking_list[] = [
                                'fencer_id' => $f['fencers'][0]->ID,
                                'Platz' => 0,
                                'Name' => get_field('name', $f['fencers'][0]->ID),
                                'Zugehörig' => get_fields($f['fencers'][0]->ID)['club'][0]->post_title,
                                'Punkte' => $f['points']
                            ];
                        } else {
                            foreach ($ranking_list as $k => $rl) {
                                if ($rl['fencer_id'] == $f['fencers'][0]->ID && $f['points']) {
                                    $ranking_list[$k]['Punkte'] += $f['points'];
                                }
                            }
                        }
                    }
                }
            }

        }
        if (!empty($ranking_list)) {
            // order the fencers desc by points
            usort($ranking_list, 'cmp_points');

            // determine each fencers rank
            $rank = 0;
            foreach ($ranking_list as $k => $rl) {
                if ($rank === 0) {
                    $ranking_list[$k]['Platz'] = ++$rank;
                } else {
                    if ($rl['Punkte'] != $ranking_list[$k - 1]['Punkte']) {
                        $ranking_list[$k]['Platz'] = ++$rank;
                    } else {
                        $ranking_list[$k]['Platz'] = $rank;
                    }
                }
            }
            // reduce array size if necessary
            $ranking_list = array_slice($ranking_list, 0, $param['anzahl'] ? $param['anzahl'] : count($ranking_list));


            // creating the table
            $rows = '';
            foreach ($ranking_list as $k => $row) {
                unset($ranking_list[$k]['fencer_id']);
                unset($row['fencer_id']);
                array_map('htmlentities', $row);
                $rows .= '<tr><td>' . implode('</td><td>', $row) . '</td></tr>';
            }

            $head = $param['kopfzeile'] ? '<tr><td class="table-head">' . implode('</td><td class="table-head">', array_keys(current($ranking_list))) . '</td></tr>' : '';

            $html = '<table class="wp-block-table is-style-stripes"><tbody>' . $head . $rows . '</tbody></table>';
        }

        return $param['id'] ? $html : '<p style="color: red; font-weight: bold">Fehler im Shortcode! Bitte ID mit angeben:
        <code style="border:none;">[rangliste id="1234"]</code></p>';
    }
}

add_shortcode('rangliste', 'rankingtable_function');


/**
 * Creates the shortcode to display tournaments (lists by year, results, stuff like that)
 *
 * @param $param
 * @return string
 */
function tournaments_function($param) {
    // set default values
    $param =
        shortcode_atts(
            array (
                'id' => 0,
                'jahr' => 0, // default: no specific year
                'rangliste' => 0 // defaul: no specific ranking list
            ), $param
        );

    if ($param['id']) {
        // case 1: get a single tournament
        $tournament = get_tournament_data_by_id($param['id']);

        // build html
        $html = "<h4>" . $tournament['name'] . " - " . $tournament['weapon'] . " - " . $tournament['date']['start']->format('Y') . "</h4>";
        $head = '<tr><td class="table-head">Platz</td><td class="table-head">Name</td><td class="table-head">Zugehörig</td><td class="table-head">Punkte</td></tr>';
        $rows = '';

        foreach ($tournament['results'] as $res) {
            array_map('htmlentities', $res);
            $rows .= '<tr><td>' . implode('</td><td>', $res) . '</td></tr>';
        }

        $html .= '<table class="wp-block-table is-style-stripes"><tbody>' . $head . $rows . '</tbody></table>';

    }
    else if ($param['jahr']) {
        // case 2: get list by year
        $tournaments = get_tournament_data_by_year($param['jahr']);

        $html = '<h4>DDHF Turniere ' . $param['jahr'] . '</h4>';
        $html .= '<ol>';
        setlocale(LC_TIME, 'de_DE', 'deu_deu');
        foreach ($tournaments as $tnm) {
            $start = new DateTime($tnm['date']['from'], new DateTimeZone("Europe/Berlin"));
            $end = new DateTime($tnm['date']['to'], new DateTimeZone("Europe/Berlin"));

            if ($start->diff($end)->format('%d') > 0) {
                // multi day event
                if ($start->format('m') == $end->format('m'))
                    // start and end in same month
                    $date = $start->format('j.') . ' bis ' . translate_months($end->format('j. F'));
                else
                    // start and end in different months
                    $date = translate_months($start->format('j. F')) . ' bis ' . translate_months($end->format('j. F'));
            } else {
                // one day event
                $date = translate_months($end->format('j. F'));
            }
            $link = $tnm['url'] ? '<a href="' . $tnm['url'] . '" target="_blank" rel="noopener">' . $tnm['name'] . '</a>' : $tnm['name'];

            $html .= '<li>' . $link . ' &ndash; ' . $tnm['city'] . ' &ndash; ' . $date . '</li>';
        }
        $html .= '</ol>';

    } else if ($param['rangliste']) {
        // case 3: get all tournaments of a certain ranking list
        $tournaments = get_tournament_data_by_rankinglist_id($param['rangliste']);

        $html = '<h4>DDHF Turniere ' . $tournaments[0]['weapon'] . ' &ndash; ' . $tournaments[0]['type'] . '</h4>';
        $html .= '<ol>';
        setlocale(LC_TIME, 'de_DE', 'deu_deu');
        foreach ($tournaments as $tnm) {
            $start = new DateTime($tnm['date']['from'], new DateTimeZone("Europe/Berlin"));
            $end = new DateTime($tnm['date']['to'], new DateTimeZone("Europe/Berlin"));

            if ($start->diff($end)->format('%d') > 0) {
                $date = $start->format('j.') . ' bis ' . translate_months($end->format('j. F'));
            } else {
                $date = translate_months($end->format('j. F'));
            }
            $link = $tnm['url'] ? '<a href="' . $tnm['url'] . '" target="_blank" rel="noopener">' . $tnm['name'] . '</a>' : $tnm['name'];
            $html .= '<li>' . $link . ' &ndash; ' . $tnm['city'] . ' &ndash; ' . $date . '</li>';
        }
        $html .= '</ol>';

    } else {
        // undefined: something went wrong
        $html = '<p style="color: red; font-weight: bold">Fehler: Shortcode mit den angegebenen Parametern nicht definiert.</p>';
    }

    return $html;
}

add_shortcode('turnier', 'tournaments_function');

/**
 * Creates the shortcode to display e-mail-addresses from
 *
 * @param $param
 * @return string
 */
function addresses_function($param) {
    // set default values
    $param =
        shortcode_atts(
            array (
                'an' => '',
                'anonym' => '0'
            ), $param
        );
    $default_mail = 'info@ddhf.de';
    $data = $param['anonym'] ? NULL : get_fields(582);

    switch ($param['an']) {
        case 'präsi':
            $mail = $data ? $data['praesidium']['president']['e-mail'] : $default_mail;
            break;
        case 'vize-sport':
            $mail = $data ? $data['praesidium']['vize_sport']['e-mail'] : $default_mail;
            break;
        case 'vize-bildung':
            $mail = $data ? $data['praesidium']['vize_forschung']['e-mail'] : 'bildung@ddhf.de ';
            break;
        case 'vize-finanzen':
            $mail = $data ? $data['praesidium']['vize_wirtschaft']['e-mail'] : 'kasse@ddhf.de ';
            break;
        case 'kommunikation':
            $mail = $data ? $data['erweitertes_praesidium']['internationale_kommunikation']['e-mail'] : 'ambassador@ddhf.de ';
            break;
        case 'aktiv':
            $mail = $data ? $data['erweitertes_praesidium']['aktivenvertretung']['e-mail'] : 'aktiv@ddhf.de ';
            break;
        case 'jugend':
            $mail = $data ? $data['erweitertes_praesidium']['jugend']['e-mail'] : 'jugend@ddhf.de';
            break;
        case 'gleichstellung':
            $mail = $data ? $data['erweitertes_praesidium']['gleichstellung']['e-mail'] : 'gleichstellung@ddhf.de';
            break;
        case 'webmaster':
            $mail = $data ? $data['aemter']['webmaster']['e-mail'] : 'webmaster@ddhf.de';
            break;
        case 'datenschutz':
            $mail = $data ? $data['aemter']['datenschutz']['e-mail'] : 'datenschutz@ddhf.de';
            break;
        case 'presse':
            $mail = $data ? $data['aemter']['presse']['e-mail'] : 'presse@ddhf.de';
            break;
        default:
            $mail = $default_mail;
    }

    return email_format($mail);
}
add_shortcode('email', 'addresses_function');
