<?php

function cmp_points($a, $b) {
    return $b['Punkte'] - $a['Punkte'];
}

function cmp_ranks($a, $b) {
    return $a['rank'] - $b['rank'];
}

function cmp_time($a, $b) {
    $a = new DateTime( $a['date']['from']);
    $b = new DateTime($b['date']['from']);
    if ($a == $b) {
        return 0;
    }

    return ($a == $b ? 0 : $a < $b) ? -1 : 1;
}

function translate_months($string) {
    $months_en = ['January', 'February', 'March', 'May', 'June', 'July', 'October', 'December'];
    $months_de = ['Januar', 'Februar', 'März', 'Mai', 'Juni', 'Juli', 'Oktober', 'Dezember'];

    return str_replace($months_en, $months_de, $string);
}

function get_tournament_data_by_id($id) {
    // necessary variables
    $tournament_data = [];
    $raw_data = get_fields($id);
    // fill results with data
    $tournament_data['name'] = $raw_data['name'];
    // @todo club doesn't work like that if more than one
    $tournament_data['club'] = $raw_data['organizing_club'][0]->post_title;
    $tournament_data['weapon'] = get_term($raw_data['weapon'], 'weapon')->name;
    // time
    $tournament_data['date']['start'] = new DateTime($raw_data['date']['from'], new DateTimeZone('Europe/Berlin'));
    $tournament_data['date']['end'] = new DateTime($raw_data['date']['to'], new DateTimeZone('Europe/Berlin'));
    // results
    foreach ($raw_data['results'] as $r) {

        if (!empty($r['fencers'][0]->post_title)) {
            $club_name = '';
            if (!empty($r['fencers'][0])) {
                if (!empty($club = get_field('club', $r['fencers'][0]->ID))) {
                    $club_name = get_field('name', $club[0]->ID);
                }
                elseif (!empty($club = get_field('club_non_ddhf', $r['fencers'][0]->ID))) {
                    $club_name = $club;
                }
            }

            $results[] = [
                'rank' => $r['rank'],
                'name' => $r['fencers'][0]->post_title,
                'club' => $club_name,
                'points' => $r['points'] ?: '0'];
            }
        }

    // sorting
    if (!empty($results))
        usort($results, 'cmp_ranks');

    $tournament_data['results'] = $results;

    return $tournament_data;
}

function get_tournament_data_by_year($year) {
    // necessary variables
    $tournament_ids = wp_list_pluck(get_posts([
        'post_type' => 'turnier',
        'post_status' => 'publish',
        'numberposts' => -1, // all
    ]), 'ID');
    $raw_data = [];
    $tournaments = [];

    // @todo raw_data should not be the return variable!

    foreach ($tournament_ids as $k => $tid) {
        $raw_data[] = get_fields($tid);
        // @todo works if there is only one organizer
        $raw_data[$k]['organizing_club'] = $raw_data[$k]['organizing_club'][0]->post_title;
        unset($raw_data[$k]['results']);
    }
    // @todo should be doable with one foreach instead of removing not entering them in the first place
    foreach ($raw_data as $k => $rd) {
        if (in_array($rd['name'] . $rd['date']['from'], $tournaments) || (new DateTime($rd['date']['from'], new DateTimeZone('Europe/Berlin')))->format('Y') != $year) {
            unset($raw_data[$k]);
        }
        $tournaments[] = $rd['name'] . $rd['date']['from'];

    }

    // sorting the data
//    echo "<pre>";var_dump($raw_data);echo "</pre>";
    usort($raw_data, 'cmp_time');

    // @todo there is a problem with the current data structure. there should be events and these events should have tournaments.
    // @todo right now we have to compare them by name to reduce the tournaments to the event-names which is error prone

    return $raw_data;
}

function get_tournament_data_by_rankinglist_id($id) {
    // @todo this is bullshit. this function is basically the same as get_tournament_data_by_year
    // necessary variables
    $raw_data = [];
    $tournament_ids = wp_list_pluck(get_fields($id)['tournaments'], 'ID');
    $tournaments = [];

    // @todo raw_data should not be the return variable!

    foreach ($tournament_ids as $k => $tid) {
        $raw_data[] = get_fields($tid);
        // @todo works if there is only one organizer
        $raw_data[$k]['organizing_club'] = $raw_data[$k]['organizing_club'][0]->post_title;
        $raw_data[$k]['weapon'] = get_term_by('id', $raw_data[$k]['weapon'], 'weapon')->name;
        $raw_data[$k]['type'] = get_term_by('id', $raw_data[$k]['type'], 'tournament_type')->name;
        unset($raw_data[$k]['results']);
    }
    // @todo should be doable with one foreach instead of removing not entering them in the first place
    foreach ($raw_data as $k => $rd) {
        if (in_array($rd['name'], $tournaments)) {
            unset($raw_data[$k]);
        }
        $tournaments[] = $rd['name'];

    }

    // sorting the data
//    echo "<pre>";var_dump($raw_data);echo "</pre>";
    usort($raw_data, 'cmp_time');

    // @todo there is a problem with the current data structure. there should be events and these events should have tournaments.
    // @todo right now we have to compare them by name to reduce the tournaments to the event-names which is error prone

    return $raw_data;
}


function include_mathjax() {
    echo "<script src=\"https://polyfill.io/v3/polyfill.min.js?features=es6\"></script>\n<script id=\"MathJax-script\" async src=\"https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js\"></script>";
}

function email_format($email) {
    return str_replace('@', '&nbsp;[at]&nbsp;', $email);
}

function handle_downloads() {
    $downloads_data = get_field('dateien');
    $downloads_header = get_field('ueberschrift');
    $downloads_html = '';
    if (!empty($downloads_data)) {
        if (!empty($downloads_header)) {
            $downloads_html .= '<h3 class="ddhf-downloads-heading">' . $downloads_header . '</h3>';
        }

        $downloads_html .= '<div class="ddhf-downloads">';
        foreach ($downloads_data as $download) {
            $downloads_html .= '<div class="ddhf-downloads-row">';
            if ($download['datei']['type'] == 'image') {
                $attr = [
                    'src'    => $download['datei']['sizes']['download-preview-2x'],
                    'srcset' => $download['datei']['sizes']['download-preview-1x'] . ', ' . $download['datei']['sizes']['download-preview-2x'] . ' 2x'
                ];
                $downloads_html .= '<div class="ddhf-downloads-cell ddhf-downloads-image">' . wp_get_attachment_image($download['datei']['ID'], false, false, $attr) . '</div>';
            }
            else if ($download['datei']['subtype'] == 'pdf') {
                $downloads_html .= '<div class="ddhf-downloads-cell ddhf-downloads-image"><img class="ddhf-downloads-icon" src="' . get_stylesheet_directory_uri() . '/img/icon-pdf.svg' . '"></div>';
            }
            $downloads_html .= '<div class="ddhf-downloads-cell ddhf-downloads-size">' . strtoupper($download['datei']['subtype']) . ', ' . (int) ($download['datei']['filesize'] / 1024) . '  KB</div>';
            $downloads_html .= '<div class="ddhf-downloads-cell ddhf-downloads-description">' . $download['kommentar'] . '</div>';
            $downloads_html .= '<div class="ddhf-downloads-cell ddhf-downloads-icon"><a href="' . $download['datei']['link'] . '" download><img src="' . get_stylesheet_directory_uri() . '/img/icon-save-file.svg' . '"></a></div>';
            $downloads_html .= '</div>';
        }
        $downloads_html .= '</div>';
    }
    echo $downloads_html;
}